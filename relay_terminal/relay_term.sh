#!/bin/bash

echo Enter relay command {RelayNumber, On/Off}

read relayNum op

sudo rmmod ftdi_sio
sudo rmmod usbserial

sudo java -Djava.library.path=.//home/idanhahn/workarea/git/relayNode/ftd2xx.so -jar /home/idanhahn/workarea/git/relayNode/DenkoviRelayCommandLineTool_10.jar DAE000dY 4 $relayNum $op
