/* 
 * usb relay control module using node
 */

// Global variables:
var PORT = 7500;
var TAG = "usbRelay control module - ";

// Requires:
var net = require('net');
var exec = require('child_process').exec;

var server = net.createServer(function(socket){

    socket.on('connect',function(){
        console.log(TAG + "Connection to client established ");
    });


    socket.on('data',function(data){
        var dataStr = data.toString().replace('\n','');
        console.log(TAG + "Recieved " + dataStr + " Command from user");
        
        // require to remove current usb drivers:
        var usb1 = exec('rmmod "ftdi_sio"');
        var usb2 = exec('rmmod "usbserial"');
        
        if (dataStr == "GET"){
           
            console.log(TAG + "Processing Get command");
            var exeprocessGET = exec('java -Djava.library.path=./home/idanhahn/workarea/git/relayNode/ftd2xx.so -jar /home/idanhahn/workarea/git/relayNode/DenkoviRelayCommandLineTool_10.jar DAE000dY 4 all status');
            exeprocessGET.stdout.on('data',function(data){
                console.log(data);
                socket.write(data);
            });

        } else {

            var exeprocess = exec('java -Djava.library.path=.//home/idanhahn/workarea/git/relayNode/ftd2xx.so -jar /home/idanhahn/workarea/git/relayNode/DenkoviRelayCommandLineTool_10.jar DAE000dY 4 turn ' + dataStr);
            
            //execprocess.stdout.on('data',function(data){
                //socket.write("OK");
            //});
            
            //exeprocess.stderr.on('data',function(data){
            //    console.log(data);
            //    socket.write("ERR");
            //});
        
        }
    });
}).listen(PORT);
