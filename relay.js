/* 
 * usb relay control module using node
 */



// Global variables:
var PORT = 7500;
var TAG = "usbRelay control module - ";

// Requires:
var net = require('net');
var exec = require('child_process').exec;

// Server for UsbRelay:
// --------------------
var server = net.createServer(function(socket){
    
    console.log(TAG + "Starting relay.js on port: " + PORT);
    socket.on('connect',function(){
        console.log(TAG + "Connection to client established ");
    });


    socket.on('data',function(data){
        var dataStr = data.toString().replace('\n','');
        console.log(TAG + "Recieved " + dataStr + " Command from user");
        
        // require to remove current usb drivers:
        var usb1 = exec('rmmod "ftdi_sio"');
        var usb2 = exec('rmmod "usbserial"');
        
        if (dataStr == "GET"){
           
            console.log(TAG + "Processing Get command");
            var exeprocessGET = exec('java -Djava.library.path=./ftd2xx.so -jar DenkoviRelayCommandLineTool_10.jar DAE000dY 4 all status');
            exeprocessGET.stdout.on('data',function(data){
                console.log(data);
                socket.write(data);
            });

        } else {

            var exeprocess = exec('java -Djava.library.path=./ftd2xx.so -jar DenkoviRelayCommandLineTool_10.jar DAE000dY 4 turn ' + dataStr);
            
            //execprocess.stdout.on('data',function(data){
                //socket.write("OK");
            //});
            
            //exeprocess.stderr.on('data',function(data){
            //    console.log(data);
            //    socket.write("ERR");
            //});
        
        }
    });
}).listen(PORT);


// Server for Arduino relay:
// -------------------------
var AADDR = 'http://idanhahn.info.tm';
var APORT = 82;
var HPORT = 7501;
var http = require('http');
var fs = require('fs');

var options = {
    hostname: AADDR,
    port: APORT,
    method: 'POST',
}
var STAT_REQ = "STAT";



var Aserver = http.createServer( function (req, res){
    
    console.log("1");
    res.writehead(200,{'Content-Type': 'text/plain'});
    console.log("2");
    
    var req = http.request(options, function(res){
        console.log('STATUS: ' + res.statusCode);
    });
    console.log("3");
    res.end('okay');
    console.log("4");
});




