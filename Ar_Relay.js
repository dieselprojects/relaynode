// Server for Arduino relay:
// -------------------------
var http = require('http');
var fs = require('fs');
var events = require('events');
var eventEmitter = new events.EventEmitter();

// network
var APORT = 82;
var AADDR = 'idanhahn.info.tm';

//configuration
var sprinklersCfgFile = '/home/idanhahn/workarea/git/relayNode/configuration/sprinklersCfg';

// Requests:
var STAT_REQ = "x0010";



// Function make arduino request (host, port, code)
function makeHttpRequest(host, port, code){
    var TAG = "[makeHttpRequest]";
    var options = {
        host: host,
        port: port,
        path: code
    };

    var callback = function(response) {
        var str = '';
        //another chunk of data has been recieved, so append it to `str`, i don't use this at all
        response.on('data', function (chunk) {
            str += chunk;
        });

        //the whole response has been recieved, do callback
        response.on('end', function () {
            //console.log(TAG + "Respose: " + str);

        });
    };

    console.log(TAG + "Sending request: " + host + ":" + port + "/" + code);
    var req = http.request(options, callback).end();
}

//makeHttpRequest(AADDR,APORT,STAT_REQ);

// Configuration files:
// --------------------

// read sprinklers configuration file
var startSprinklers;
var durationSprinklers;
fs.readFile(sprinklersCfgFile, 'utf8', function(err,data){
    var TAG = "[ReadFile] "
    if (err){
        return console.log(err);
    }

    //console.log(TAG + data);
    var lines = data.match(/[^\r\n]+/g);
    startSprinklers = lines[0];
    durationSprinklers = lines[1];
    eventEmitter.emit('doneLoop',startSprinklers,durationSprinklers);
});

eventEmitter.on('doneLoop',handleSprinklers);

// Sprinklers logic:
// -----------------
function handleSprinklers(startTime,duration){
    var TAG = "[handleSprinklers] ";
    // parse startTime:
    console.log(TAG + "time, Duration " + startTime +"," + duration )
    var startTimeArr = String(startTime).split(':');
    var startHour   = startTimeArr[0];
    var startMinute = startTimeArr[1];
    var startSecond = 0;
    console.log(TAG + "Start: {" + startHour + ","+ startMinute + ","+ startSecond + "}");


    // wait until start time
    var currentTime = new Date();
    var curHour    = currentTime.getHours()    ;
    var curMinute  = currentTime.getMinutes()  ;
    var curSecond  = currentTime.getSeconds()  ;
    console.log(TAG + "Current: {" + curHour + ","+ curMinute + ","+ curSecond + "}");

    var waitTimeInSeconds = (startHour*60*60 + startMinute*60 + startSecond) - (curHour*60*60 + curMinute*60 + curSecond);
    if (waitTimeInSeconds < 0){
        // wait until next day
        waitTimeInSeconds+=86400;
    }
    console.log(TAG + "time to wait: " + waitTimeInSeconds);
    setTimeout(function(){
        console.log(TAG + "done waiting, preparing request");
        makeHttpRequest(AADDR,APORT,"x0010");
        console.log(TAG + "Sprinklers on for " + duration);
        eventEmitter.emit('endR');

    },waitTimeInSeconds*1000);
   
}

eventEmitter.on('endR',function(){
    setTimeout(function(){
        console.log("Closing sprinklers");
        makeHttpRequest(AADDR,APORT,"x0000");
        eventEmitter.emit('doneLoop',startSprinklers,durationSprinklers);
        console.log("Done loop");
    },durationSprinklers *1000);
});

